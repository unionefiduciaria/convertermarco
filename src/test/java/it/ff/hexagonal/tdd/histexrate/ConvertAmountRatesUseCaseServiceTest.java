package it.ff.hexagonal.tdd.histexrate;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountRatesUseCase;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.service.ConvertAmountRatesUseCaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@ExtendWith({SpringExtension.class})
class ConvertAmountRatesUseCaseServiceTest {

    @InjectMocks        // cosa voglio testare?
    private ConvertAmountRatesUseCaseService convertAmountRatesUseCaseService;

    @MockBean           // cosa mi serve simulare per testare?
    private HistoricalExchangeRatesPort historicalExchangeRatesPort;


    @BeforeEach
    public void  setUp() {   MockitoAnnotations.initMocks(this);}


    /*TODO: testasre  il servizio di conversione verificando che:
     *  i metodi restituiscano i valori corretti passando la data,
     *  senza la data,
     *  sd*/



//verifico la ricerca con la data
    @Test
    void getExchangeRatesConvert_testValiditaConData() {
        List<ExchangeRate> listTestDate = new ArrayList<ExchangeRate>();
        listTestDate.add(new ExchangeRate().rate(BigDecimal.valueOf(0.82)).currency("USD").date(LocalDate.parse("2020-10-01")));
        listTestDate.add(new ExchangeRate().rate(BigDecimal.valueOf(0.50)).currency("TRY").date(LocalDate.parse("2020-10-01")));


        when (historicalExchangeRatesPort.findAllByIdentity_idCurrencyAndIdentity_idDate("USD", "TRY", LocalDate.parse("2020-10-01")))
                .thenReturn(listTestDate);

        List<ExchangeRate> l = convertAmountRatesUseCaseService.getExchangeRatesConvert("USD","TRY", LocalDate.parse("2020-10-01"));

        assertThat(l).hasSize(2);
        assertThat(l.get(0).getCurrency()).isEqualTo("USD");
        assertThat(l.get(1).getCurrency()).isEqualTo("TRY");
        assertThat(l.get(0).getDate()).isEqualTo(LocalDate.parse("2020-10-01"));
        assertThat(l.get(1).getDate()).isEqualTo(LocalDate.parse("2020-10-01"));

        /*TODO: verificare che torni una lista di 2 elementi,
         *  che siano  quelli che sto cercando,
         *  alla data e con il rate*/

    }

//verifico il metodo senza data
    @Test
    void getExchangeRateConvert_testValiditaSenzaData(){

        List<ExchangeRate> listTestDate = new ArrayList<ExchangeRate>();

        listTestDate.add(new ExchangeRate().rate(BigDecimal.valueOf(0.88)).currency("USD").date(LocalDate.parse("2020-10-05")));
        listTestDate.add(new ExchangeRate().rate(BigDecimal.valueOf(0.55)).currency("TRY").date(LocalDate.parse("2020-10-05")));

        when(historicalExchangeRatesPort.findAllByIdentity_idCurrency("USD", "TRY"))
                .thenReturn(listTestDate);

        List<ExchangeRate> l = convertAmountRatesUseCaseService.getExchangeRatesConvert("USD","TRY", null);


        assertThat(l).hasSize(2);
        assertThat(l.get(0).getCurrency()).isEqualTo("USD");
        assertThat(l.get(1).getCurrency()).isEqualTo("TRY");
        assertThat(l.get(0).getDate()).isEqualTo(l.get(1).getDate());

        /*TODO: verifico che funzioni la ricerca senza date,
         *  e mi restituisca una coppia di elementi alla stessa data più recente per entranbi,
         *  che siano correti i valori*/

    }
}