package it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;

import java.util.List;

public class NotFoundException extends RuntimeException {


    private List<ErrorDetail> errorDetail;


    public List<ErrorDetail> getErrorDetail() {
        return errorDetail;
    }

    public NotFoundException(Exception e) {
        super(e);
    }

    public NotFoundException(String message) {
        super(message);
    }

    public  NotFoundException(List<ErrorDetail> errorDetail){

        this.errorDetail = errorDetail;
    }
}
