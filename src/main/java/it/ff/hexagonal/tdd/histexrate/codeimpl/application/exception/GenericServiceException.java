package it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;

import java.util.List;

public class GenericServiceException extends RuntimeException {

    private List<ErrorDetail> errorDetailList;


    public GenericServiceException(Exception e) {
        super(e);
    }
    public GenericServiceException(List<ErrorDetail> errorDetailList) {
        this.errorDetailList = errorDetailList;
    }

    public GenericServiceException(String message, List<ErrorDetail> errorDetailList) {
        super(message);
        this.errorDetailList = errorDetailList;
    }

    public GenericServiceException(String message, Throwable cause, List<ErrorDetail> errorDetailList) {
        super(message, cause);
        this.errorDetailList = errorDetailList;
    }

    public GenericServiceException(Throwable cause, List<ErrorDetail> errorDetailList) {
        super(cause);
        this.errorDetailList = errorDetailList;
    }


    public void setErrorDetailList(List<ErrorDetail> errorDetailList) {
        this.errorDetailList = errorDetailList;
    }

    public List<ErrorDetail> getErrorDetailList() {
        return errorDetailList;
    }


}
