package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.api;

import it.ff.hexagonal.tdd.histexrate.codegen.api.ConvertToApiDelegate;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ConvertedAmount;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.NotFoundException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.ServiceUnavailableException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountRatesUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component // indica a spring la peresenza della classe
public class ConvertApiDelegateImpl implements ConvertToApiDelegate {


    @Autowired
    private ConvertAmountRatesUseCase convertAmountRatesUseCase;

    @Override
    public ResponseEntity<ConvertedAmount> getConvertedAmount(String currencyFrom, String currencyTo, BigDecimal amount, LocalDate date) {


        ConvertedAmount response = new ConvertedAmount();
        List<ErrorDetail> listOfErrorNotFound = new ArrayList<ErrorDetail>();
        List<ErrorDetail> listOfErrorBadRequest = new ArrayList<ErrorDetail>();

        try {

//            richiedo i dati al servizio
            List<ExchangeRate> l = convertAmountRatesUseCase.getExchangeRatesConvert(currencyFrom, currencyTo, date);


//            verifico che la data si valida
            if (l.isEmpty()) {
                listOfErrorNotFound.add(new ErrorDetail("date", "null", "nessun tasso di cambio valido trovatoa"));
            }

//            verifico che il quantitativo di cambio non sia zero
            if (amount.intValueExact() == 0) {
                listOfErrorBadRequest.add(new ErrorDetail("amount", "null", "amount non valido"));
            }

//            TODO: questa parte non mi serve più perchè la query mi restituisce nullo o due valori
//            verifico che ci siano entrambe le valute in memoria
            if (l.get(0) == null || l.get(1) == null) {
                listOfErrorNotFound.add(new ErrorDetail(currencyFrom, date.toString(), l.get(0).toString()));
                listOfErrorNotFound.add(new ErrorDetail(currencyFrom, date.toString(), l.get(1).toString()));
            }

//            se la lista dettagli non è vuota raccogli tutti i dettagli e lancia l'eccezzione
            if (!listOfErrorNotFound.isEmpty()) throw new NotFoundException(listOfErrorNotFound);
            if (!listOfErrorBadRequest.isEmpty()) throw new NotFoundException(listOfErrorNotFound);


//          TODO:verifico le posizioni e calcolo

            BigDecimal tot = null;

            BigDecimal t = l.get(0).getRate();
            BigDecimal r = l.get(1).getRate();

            if (l.get(0).getCurrency().equals(currencyFrom)) {
                tot = t.divide(r, RoundingMode.HALF_UP).multiply(amount);
            } else {
                tot = r.divide(t, RoundingMode.HALF_UP).multiply(amount);
            }

            response = new ConvertedAmount();
            response.setAmount(tot);
            response.setCurrency(currencyTo);
            response.exchangeRates(l);


        }


        catch (ServiceUnavailableException | NotFoundException ex) {
            throw ex;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }
}
