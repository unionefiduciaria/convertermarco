package it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;

import java.util.List;

public class BadRequestException extends RuntimeException {

    private List<ErrorDetail> errorDetail;

    public BadRequestException(List<ErrorDetail> errorDetail) {
        this.errorDetail = errorDetail;
    }

    public BadRequestException(String message) {
        super(message);
    }

    public List<ErrorDetail> getErrorDetail() {
        return errorDetail;
    }




}
