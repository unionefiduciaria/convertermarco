package it.ff.hexagonal.tdd.histexrate.codeimpl.application.service;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.exception.NotFoundException;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.HistoricalExchangeRatesPort;
import it.ff.hexagonal.tdd.histexrate.codeimpl.application.port.ConvertAmountRatesUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertAmountRatesUseCaseService implements ConvertAmountRatesUseCase {

    @Autowired
    private HistoricalExchangeRatesPort historicalExchangeRatesPort;

    @Override
    public List<ExchangeRate> getExchangeRatesConvert(String currencyFrom, String currencyTo,  LocalDate date)throws NotFoundException {


        /*TODO:scelgo il metodo per la query sulla base dei dati che ho e restituisco una lista di due ExchangeRate
         *  contenete un tasso di cambio per valuta alla stessa data*/


        List<ExchangeRate> result = new ArrayList<>();
        List<ErrorDetail> listOfErrorNotFound = new ArrayList<ErrorDetail>();



//            eseguo la ricerca con solo la currency, restituisco la data più recente che ho trovato
        if (date == null) {
            result = historicalExchangeRatesPort.findAllByIdentity_idCurrency(currencyFrom, currencyTo);
            if(result.isEmpty()) {
                listOfErrorNotFound.add(new ErrorDetail("ExchangeRates", "null", "nessuna ricorrenza per le valute"));
            }
        }



//            eseguo la ricerca con la valuta e la data
        else {
            result = historicalExchangeRatesPort.findAllByIdentity_idCurrencyAndIdentity_idDate(currencyFrom, currencyTo, date);
            if(result.isEmpty()) {
                listOfErrorNotFound.add(new ErrorDetail("ExchangeRates", "null", "nessuna ricorrenza a questa data"));
            }
        }

        if(!listOfErrorNotFound.isEmpty()) throw new NotFoundException(listOfErrorNotFound);


        return result;
    }
}