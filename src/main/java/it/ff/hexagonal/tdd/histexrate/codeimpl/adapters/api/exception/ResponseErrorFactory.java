package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.api.exception;

import com.sun.istack.Nullable;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ErrorDetail;
import it.ff.hexagonal.tdd.histexrate.codegen.model.ResponseError;

import java.util.List;

/**
 * Build the error object used in response object
 */
public class ResponseErrorFactory {
    private  ResponseErrorFactory() {
    }

    public static ResponseError create(String code, String description) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(code);
        responseError.setDescription(description);

        return responseError;
    }
    public static ResponseError create(String code, String description, List<ErrorDetail> errorDetailList) {
        ResponseError responseError = new ResponseError();
        responseError.setCode(code);
        responseError.setDescription(description);
        responseError.errorList(errorDetailList);

        return responseError;
    }

}
