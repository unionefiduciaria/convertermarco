package it.ff.hexagonal.tdd.histexrate.codeimpl.adapters.persistence;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

public interface HistExRateRepository extends PagingAndSortingRepository<HistExRate, HistExRateIdentity> {

//    @Query(value = "SELECT TOP 1 s.* "
//            + "FROM	HistExRate s "
//            + "WHERE "
//            + "currencyCode	= ?1 AND	dateRate <= ?2 "
//            + "ORDER BY	dateRate DESC", nativeQuery = true)
//
//
//   public HistExRate getCambioAtDate(String div, LocalDate date);

    List<HistExRate> findAllByIdentity_idCurrencyIgnoreCase(String currency, Pageable p);

    List<HistExRate> findAllByIdentity_idDate(LocalDate dateRate, Pageable p);

    List<HistExRate> findAllByIdentity_idCurrencyAndIdentity_idDate(String currency, LocalDate date, Pageable p);




    @Query(value = "SELECT * FROM HIST_EX_RATE WHERE ID_CURRENCY = ?1 OR ID_CURRENCY = ?2 GROUP BY ID_DATE, ID_CURRENCY HAVING ID_DATE = ?3 " ,nativeQuery = true)
    List<HistExRate> trovaConData(String currencyFrom, String currencyTo, LocalDate date);


    @Query(value =  "SELECT TOP 2 v1.* FROM HIST_EX_RATE AS v1 "+
                    "JOIN HIST_EX_RATE AS v2 ON v1.ID_DATE = v2.ID_DATE "+
                    "WHERE (v1.ID_CURRENCY = ?1 OR v1.ID_CURRENCY = ?2) "+
                    "AND (v2.ID_CURRENCY = ?1 OR v2.ID_CURRENCY = ?2) "+
                    "AND v1.ID_CURRENCY <> v2.ID_CURRENCY "+
                    "GROUP BY  v1.ID_DATE, v1.ID_CURRENCY "+
                    "ORDER BY ID_DATE DESC", nativeQuery = true)
    List<HistExRate> trovaSenzaData(String currencyFrom, String currencyTo);


    @Transactional
    @Modifying
    @Query(value = "UPDATE HistExRate SET rate=:#{#stc.rate} WHERE id_currency=:#{#stc.identity.idCurrency} AND id_date=:#{#stc.identity.idDate}")
    int updateHistExRate(@Param("stc") HistExRate HistoricalExchangeRateso);
}
