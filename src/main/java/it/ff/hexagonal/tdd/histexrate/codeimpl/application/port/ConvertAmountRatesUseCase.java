package it.ff.hexagonal.tdd.histexrate.codeimpl.application.port;

import it.ff.hexagonal.tdd.histexrate.codegen.model.ExchangeRate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface ConvertAmountRatesUseCase {

    List<ExchangeRate> getExchangeRatesConvert(String currencyFrom, String currencyTo, LocalDate date);
}
